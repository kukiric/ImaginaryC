#ifndef SEMANTICO_H
#define SEMANTICO_H

#include "Token.h"
#include "SemanticError.h"
#include "Compiler/LogPrinter.h"

class Semantico
{
public:
    virtual void executeAction(int action, const Token* token);
};

#endif
