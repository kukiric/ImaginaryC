#ifndef SYMBOL_H
#define SYMBOL_H

#include <string>
#include <memory>
#include <boost/variant.hpp>
using boost::get;

struct ScopeTreeNode;

struct Symbol
{
    struct Void {};

    typedef boost::variant<Void, int, float, std::string, char, bool> ValueType;

    enum class Type
    {
        INT,
        FLOAT,
        STRING,
        CHAR,
        BOOL,
        VOID,

        INVALID
    };

    Type type;
    ScopeTreeNode* scope;
    std::string name;
    bool isArray;
    bool isFunction;
    bool isParameter;
    bool isInitialized;
    bool isReferenced;
    int tokenPos;

    ValueType value;
    int size;

    std::string getTypeName() const;
    std::string getReferenceName() const;
    static Type parseType(std::string str);
    static std::string toString(Type type);

    Symbol(std::string name = "");
    explicit Symbol(std::string name, ValueType value);
};

typedef std::shared_ptr<Symbol> SymbolPtr;

#endif // SYMBOL_H
