#include "BipCompiler.h"

#include <cctype>
#include <sstream>

#include "Compiler/ScopeTree.h"

#define TEMP_ACC_SAVE_ADDR "1000"
#define CASE_SENSITIVE 0

// Adiciona caracteres para desambiguar letras minúsculas e maiúsculas
static std::string transformName(const std::string& input)
{
#if CASE_SENSITIVE
    std::string output;
    output.reserve(input.size() * 2);
    for(char c : input) {
        if(isalpha(c)) {
            if(isupper(c)) {
                output += ('u');
            }
            else {
                output += ('_');
            }
            output += toupper(c);
        }
        else {
            if(isdigit(c)) {
                output += ('n');
                output += c;
            }
            else {
                output += ('_');
            }
        }
    }
    return output;
#else
    return input;
#endif
}

std::string BipCompiler::compile(std::string source, std::shared_ptr<ILogPrinter> logPrinter)
{
    BaseCompiler::compile(source, logPrinter);
    return output.str();
}

SymbolPtr BipCompiler::addSymbol(Symbol symbol, const Token* token)
{
    if(symbol.type != Symbol::Type::INT) {
        error("A arquitetura BIP só suporta variáveis do tipo int", token->getPosition());
        return SymbolPtr();
    }
    else {
        return BaseCompiler::addSymbol(symbol, token);
    }
}

void BipCompiler::clear()
{
    BaseCompiler::clear();
    output.str("");
}

void BipCompiler::started()
{
    BaseCompiler::started();
}

void BipCompiler::finished()
{
    outputVariables();
    output << "\n";
    outputInstructions();
    BaseCompiler::finished();
}

void BipCompiler::outputVariables()
{
    output << ".data";
    for(SymbolPtr sym : symbols) {
        if(!sym->isFunction) {
            auto name = sym->getReferenceName();
            output << "\n";
            if(sym->isArray) {
                output << "    " << transformName(name) << ": ";
                for(int i = 0; i < sym->size; i++) {
                    if(i > 0) {
                        output << ", ";
                    }
                    output << "0";
                }
            }
            else {
                output << "    " << transformName(name) << ": " << get<int>(sym->value);
            }
#if CASE_SENSITIVE
            output << " # " << name;
#endif
        }
    }
}

void BipCompiler::outputInstructions()
{
    output << ".text";
    for(Instruction ins : instructions) {
        output << "\n    ";
        output << (int)ins.type << ".." << (int)ins.exprOp << ".." << ins.target.name;
    }
}
