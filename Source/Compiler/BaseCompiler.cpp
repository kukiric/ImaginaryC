#include "BaseCompiler.h"

#include <sstream>
#include "Compiler/Parser/Lexico.h"
#include "Compiler/Parser/Sintatico.h"
#include "Compiler/ScopeTree.h"

BaseCompiler::BaseCompiler()
    : globalScope(nullptr)
    , currentScope(nullptr)
    , opTarget(nullptr)
    , printer(nullptr)
    , expr(nullptr)
{}

BaseCompiler::~BaseCompiler()
{
    delete globalScope;
}

std::string BaseCompiler::compile(std::string source, std::shared_ptr<ILogPrinter> logPrinter)
{
    clear();
    Lexico lexico(source.c_str());
    Sintatico sintatico;
    printer = logPrinter.get();
    started();
    sintatico.parse(&lexico, this);
    finished();
    printer = nullptr;
    return "";
}

void BaseCompiler::executeAction(int action, const Token* token)
{
    switch(action) {
        case 2: // Tipo do símbolo
            tempSymbol = Symbol();
            tempSymbol.type = Symbol::parseType(token->getLexeme());
            tempSymbol.scope = currentScope;
            break;
        case 3: // Nome da variável comum
            opTarget = addSymbol(tempSymbol, token);
            break;
        case 4: // Nome de variável tipo vetor
            tempSymbol.isArray = true;
            opTarget = addSymbol(tempSymbol, token);
            break;
        case 5: // Nome da função
            tempSymbol.isFunction = true;
            addSymbol(tempSymbol, token);
            scopeSymbol = *lastSymbol();
            break;
        case 6: // Nome de parâmetro de função
            findSymbol(currentScope, token->getLexeme())->isParameter = true;
            break;
        case 10: // Nome da variável sendo atribuída
        {
            auto symbol = requireSymbol(currentScope, token);
            opTarget = symbol;
            break;
        }
        case 11: // Atribuição de valor
            opTarget->isInitialized = true;
            break;
        case 18: // Operações aritméticas
        {
            if(!opTarget) {
                error("Algo deu errado! Case 18", -1);
            }
            op = token->getLexeme();
            if(op == "+") {
                expr->next = std::make_shared<Expression>(Symbol(), ExpOp::ADD);
            }
            else if(op == "-") {
                expr->next = std::make_shared<Expression>(Symbol(), ExpOp::SUB);
            }
            else {
                error("Operação não suportada", token->getPosition());
            }
            expr = expr->next.get();
            break;
        }
        case 21: // Leitura de variável
        {
            auto symbol = requireSymbol(currentScope, token);
            exprStack.top().target = *symbol;
            exprStack.top().isDynamic = true;
            if(!symbol->isFunction && !symbol->isParameter && !symbol->isInitialized) {
                std::stringstream ss;
                ss << "Uso de lixo de memória pela variável " << symbol->name << ", caminho: " << symbol->scope->getDisplayName();
                warning(ss.str().c_str(), token->getPosition());
            }
            symbol->isReferenced = true;
            if(op.empty()) {
//                addInstruction(IType::LOAD, {*symbol});
                abort();
            }
            else {
//                addInstruction(IType::OP_BINARY, op, {0, *symbol});
                abort();
            }
            break;
        }
        case 22: // Leitura de vetor
        {
            auto symbol = requireSymbol(currentScope, token);
            if(!symbol->isFunction && !symbol->isParameter && !symbol->isInitialized) {
                std::stringstream ss;
                ss << "Uso de lixo de memória pela variável " << symbol->name << ", caminho: " << symbol->scope->getDisplayName();
                warning(ss.str().c_str(), token->getPosition());
            }
            symbol->isReferenced = true;
            if(op.empty()) {
//                addInstruction(Instruction::Type::LOAD, {{*symbol, arrayOffset}});
                abort();
            }
            else {
//                addInstruction(Instruction::Type::OP_BINARY, op, {0, {*symbol, arrayOffset}});
                abort();
            }
            break;
        }
        case 25: // Uso de valor constante
        {
            /// @todo Suportar outros tipos?
            exprStack.top().target = Symbol("", std::atoi(token->getLexeme().c_str()));
            exprStack.top().isDynamic = false;
            break;
        }
        case 30: // Início de expressão
            exprStack.push(Expression());
            expr = &exprStack.top();
            break;
        case 31: // Fim de expressão
        {
            expr = &exprStack.top();
            exprStack.pop();
            // Gera as instruções de cálculo
            if(exprStack.size() == 0) {
                expr->solvePartial();
                while(expr) {
                    Instruction ins;
                    ins.exprOp = expr->op;
                    ins.target = expr->target;
                    ins.type = Instruction::Type::EXPR;
                    instructions.push_back(ins);
                    expr = expr->next.get();
                }
            }
            break;
        }
        case 100: // Tags gerais
            scopeSymbol = Symbol();
            scopeSymbol.name = token->getLexeme();
            break;
        case 0: // Abertura de escopo
        {
            currentScope = new ScopeTreeNode(currentScope, scopeSymbol);
            int newIndex = 0;
            if(currentScope->parent) {
                newIndex = currentScope->parent->children.size() - 1;
            }
            currentPath.push_back(newIndex);
            currentScope->fullPath = currentPath;
            break;
        }
        case 1: // Fechamento de escopo
            currentScope = currentScope->parent;
            currentPath.pop_back();
            break;
        default:
            break;
    }
    Semantico::executeAction(action, token);
    return;
}

std::vector<Symbol> BaseCompiler::getSymbols() const
{
    std::vector<Symbol> table;
    table.reserve(symbols.size());
    for(auto ptr : symbols) {
        table.push_back(*ptr);
    }
    return table;
}

std::shared_ptr<ScopeTreeNode> BaseCompiler::copyScopeTree() const
{
    return std::shared_ptr<ScopeTreeNode>(globalScope->copyNode());
}

void BaseCompiler::clear()
{
    delete globalScope;
    globalScope = new ScopeTreeNode(nullptr, Symbol("Global"));
    currentScope = globalScope;
}

void BaseCompiler::started()
{}

void BaseCompiler::finished()
{
    for(const auto& sym : symbols) {
        if(!sym->isFunction && !sym->isReferenced) {
            std::stringstream ss;
            ss << "Variável não utilizada: " << sym->name << ", caminho: " << sym->scope->getDisplayName();
            warning(ss.str(), sym->tokenPos);
        }
    }
}

void BaseCompiler::warning(const std::string& message, int position)
{
    if(printer) {
        printer->logWarning(message.c_str());
    }
}

void BaseCompiler::error(const std::string& message, int position)
{
    throw SemanticError(message, position);
}

SymbolPtr BaseCompiler::addSymbol(Symbol symbol, const Token* token)
{
    Symbol found;
    symbol.name = token->getLexeme();
    symbol.tokenPos = token->getPosition();
    if(!currentScope->isUnique(symbol, &found)) {
        std::stringstream ss;
        ss << "Variável já declarada: '" << symbol.name << "'";
        ss << "\n               Nota: posição " << symbol.tokenPos << " (primeira declaração)";
        error(ss.str().c_str(), token->getPosition());
    }
    symbol.scope = currentScope;
    auto symbolPtr = std::make_shared<Symbol>(symbol);
    currentScope->variables.push_back(symbolPtr);
    symbols.push_back(symbolPtr);
    return symbolPtr;
}

SymbolPtr BaseCompiler::requireSymbol(ScopeTreeNode* scope, const Token* token)
{
    auto symbol = findSymbol(scope, token->getLexeme());
    if(!symbol) {
        error("Uso de variável não declarada", token->getPosition());
    }
    return symbol;
}

SymbolPtr BaseCompiler::findSymbol(ScopeTreeNode* scope, const std::string& name)
{
    if(scope == lastSymbol()->scope && name == lastSymbol()->name) {
        return lastSymbol();
    }
    while(scope) {
        for(auto& symbol : symbols) {
            if(symbol->scope == scope && symbol->name == name) {
                return symbol;
            }
        }
        scope = scope->parent;
    }
    return nullptr;
}

SymbolPtr BaseCompiler::lastSymbol()
{
    if(!symbols.empty()) {
        return symbols[symbols.size() - 1];
    }
    return nullptr;
}
