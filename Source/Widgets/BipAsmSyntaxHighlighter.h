#ifndef BIPASMSYNTAXHIGHLIGHTER_H
#define BIPASMSYNTAXHIGHLIGHTER_H

#include <QSyntaxHighlighter>

/// @brief Realçador de sintáxe da linguagem Assembly do BIP IV
class BipAsmSyntaxHighlighter : public QSyntaxHighlighter
{
    Q_OBJECT

public:
    explicit BipAsmSyntaxHighlighter(QObject* parent = 0);
    ~BipAsmSyntaxHighlighter();

protected:
    void highlightBlock(const QString& text) override;
};

#endif // BIPASMSYNTAXHIGHLIGHTER_H
