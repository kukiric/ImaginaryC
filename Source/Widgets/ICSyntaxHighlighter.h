#ifndef ICSYNTAXHIGHLIGHTER_H
#define ICSYNTAXHIGHLIGHTER_H

#include <QSyntaxHighlighter>

/// @brief Realçador de sintáxe do Imaginary C (Linguagem principal)
class ICSyntaxHighlighter : public QSyntaxHighlighter
{
    Q_OBJECT

public:
    explicit ICSyntaxHighlighter(QObject* parent = 0);
    ~ICSyntaxHighlighter();

    bool highlightComments;

protected:
    void highlightBlock(const QString& text) override;
};

#endif // ICSYNTAXHIGHLIGHTER_H
