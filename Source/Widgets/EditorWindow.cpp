#include "EditorWindow.h"
#include "ui_EditorWindow.h"

#include <QTextEdit>
#include <QFileDialog>
#include <QFileInfo>
#include <QDateTime>
#include <QTextStream>
#include <QSpacerItem>
#include <QComboBox>
#include <QLabel>

#include "Widgets/ICSyntaxHighlighter.h"
#include "Widgets/BipAsmSyntaxHighlighter.h"
#include "Widgets/ResultsWindow.h"
#include "Compiler/Parser/Lexico.h"
#include "Compiler/Parser/Sintatico.h"
#include "CompilerFactories.h"

EditorWindow::EditorWindow(QWidget* parent)
    : QMainWindow(parent)
    , ui(new Ui::EditorWindow)
    , isDirty(false)
{
    ui->setupUi(this);
    highlighter = new ICSyntaxHighlighter(ui->editor);
    highlighter->setDocument(ui->editor->document());
    resultsWindow = new ResultsWindow();

    // Espaçador no centro
    auto spacerWidget = new QWidget(this);
    spacerWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    ui->toolBar->addWidget(spacerWidget);

    // Texto descritivo
    auto compilerLabel = new QLabel(this);
    compilerLabel->setText("Compilador: ");
    ui->toolBar->addWidget(compilerLabel);

    // Seleção de modo de compilação
    compilerSelection = new QComboBox(this);
    for(auto factory : CompilerFactories::get()) {
        QVariant userData;
        userData.setValue(factory.get());
        compilerSelection->addItem(factory->getDescription(), userData);
    }
    ui->toolBar->addWidget(compilerSelection);

    // Posiciona o botão de limpar dentro do console
    ui->consoleGrid->removeWidget(ui->clearButton);
    ui->consoleGrid->addWidget(ui->clearButton, 0, 0, Qt::AlignRight | Qt::AlignBottom);

    // Abre um arquivo vazio
    actionNewFile();

    // Sinais
    connect(ui->actionNovo, SIGNAL(triggered()), this, SLOT(actionNewFile()));
    connect(ui->actionAbrir, SIGNAL(triggered()), this, SLOT(actionOpenFile()));
    connect(ui->actionSalvar, SIGNAL(triggered()), this, SLOT(actionSaveFile()));
    connect(ui->actionCompilar, SIGNAL(triggered()), this, SLOT(actionCompile()));
    connect(ui->editor, SIGNAL(textChanged()), this, SLOT(setDirty()));
    connect(ui->clearButton, SIGNAL(clicked()), this, SLOT(clearErrors()));
}

EditorWindow::~EditorWindow()
{
    delete resultsWindow;
    delete ui;
}

QMessageBox::StandardButton EditorWindow::confirmSaveDialog()
{
    auto title = QString("Salvar %1?").arg(ui->editor->documentTitle());
    auto msg = QString("As alterações não salvas serão perdidas!");
    return QMessageBox::question(this, title, msg, QMessageBox::StandardButtons(QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel));
}

void EditorWindow::openFile(const QString& path)
{
    if(!path.isEmpty()) {
        QFile file(path);
        if(file.open(QFile::ReadOnly)) {
            // Arquivo aberto com sucesso
            QByteArray data = file.readAll();
            ui->editor->setPlainText(data);
            ui->editor->setDocumentTitle(QFileInfo(file).fileName());
            isDirty = false;
            updateWindowTitle();
            resultsWindow->clear();
        }
        else {
            // Erro na abertura
            QMessageBox::critical(this, QString("Erro abrindo \"%1\"").arg(path), "Não foi possível abrir o arquivo para leitura");
        }
    }
}

void EditorWindow::setCompiler(const QString& name)
{
    for(int i = 0; i < compilerSelection->count(); i++) {
        auto data = compilerSelection->itemData(i).value<BaseCompilerFactory*>();
        if(data->getName() == name) {
            compilerSelection->setCurrentIndex(i);
            return;
        }
    }
}

static QTextEdit::ExtraSelection createErrorSelection(QTextCursor cursor, int errorPos)
{
    QTextCharFormat format;
    cursor.setPosition(errorPos);
    cursor.select(QTextCursor::WordUnderCursor);
    format.setBackground(QColor(Qt::red).lighter());
    format.setForeground(QColor(Qt::white));
    return {cursor, format};
}

void EditorWindow::actionNewFile()
{
    if(isDirty) {
        auto save = confirmSaveDialog();
        if(save == QMessageBox::Cancel) {
            return;
        }
        if(save == QMessageBox::Yes) {
            actionSaveFile();
        }
    }
    clearErrors();
    ui->editor->clear();
    ui->editor->setText("int main() {\n    \n}");
    ui->editor->setDocumentTitle("novo.ic");
    auto cur = ui->editor->textCursor();
    cur.setPosition(17);
    ui->editor->setTextCursor(cur);
    isDirty = false;
    updateWindowTitle();
    resultsWindow->clear();
}

void EditorWindow::actionOpenFile()
{
    if(isDirty) {
        auto save = confirmSaveDialog();
        if(save == QMessageBox::Cancel) {
            return;
        }
        if(save == QMessageBox::Yes) {
            actionSaveFile();
        }
    }
    clearErrors();
    QString filename = QFileDialog::getOpenFileName(this, QString(), QString(), "Código fonte (*.ic);;Todos os arquivos (*)");
    openFile(filename);
}

void EditorWindow::actionSaveFile()
{
    QFileDialog dialog(this);
    dialog.setDefaultSuffix(".ic");
    dialog.setNameFilter("Código fonte (*.ic);;Todos os arquivos (*)");
    dialog.setFileMode(QFileDialog::ExistingFile);
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    dialog.exec();
    if(!dialog.selectedFiles().empty()) {
        QString filename = dialog.selectedFiles()[0];
        QFile file(filename);
        if(file.open(QFile::WriteOnly)) {
            // Arquivo aberto com sucesso
            ui->editor->setDocumentTitle(QFileInfo(file).fileName());
            QByteArray data = ui->editor->toPlainText().toUtf8();
            file.write(data);
            isDirty = false;
            updateWindowTitle();
        }
        else {
            // Erro na abertura
            QMessageBox::critical(this, QString("Erro abrindo \"%1\"").arg(filename), "Não foi possível abrir o arquivo para escrita");
        }
    }
}

void EditorWindow::actionCompile()
{
    QString errorType;
    QString editorText = ui->editor->toPlainText();
    QList<QTextEdit::ExtraSelection> selections;

    try {
        try {
//            QTextStream(stdout) << "\n" <<
            if(!ui->console->toPlainText().isEmpty()) {
                ui->console->breakLine();
            }
            ui->console->showMessage("Compilação iniciada", "");
            const QVariant& factory = compilerSelection->currentData();
            if(!factory.canConvert<BaseCompilerFactory*>()) {
                abort();
            }
            auto compiler = factory.value<BaseCompilerFactory*>()->create();
            auto output = compiler->compile(editorText.toStdString(), ui->console->getPrinter());
            ui->console->showMessage("Compilação finalizada com sucesso!", "", ConsoleWindow::Success);
            resultsWindow->copyDataFrom(compiler.get());
            resultsWindow->setAssemblyText(QString(output.c_str()));
            openResultsWindow();
        }
        catch(LexicalError&) {
            errorType = "léxico";
            throw;
        }
        catch(SyntaticError&) {
            errorType = "sintático";
            throw;
        }
        catch(SemanticError&) {
            errorType = "semântico";
            throw;
        }
    }
    // Análise geral dos erros
    catch(AnalysisError& error) {
        auto block = ui->editor->document()->findBlock(error.getPosition());
        int line = block.firstLineNumber() + 1;
        int column = error.getPosition() - block.position() + 1;
        ui->console->showMessage(QString("Erro %1: ").arg(errorType), error.getMessage(), ConsoleWindow::Error);
        ui->console->showMessage("Posição: ", QString("Linha %1, coluna %2").arg(line).arg(column), ConsoleWindow::Info);
        selections.append(createErrorSelection(ui->editor->textCursor(), error.getPosition()));
    }
    ui->editor->setExtraSelections(selections);
}

void EditorWindow::setDirty(bool value)
{
    isDirty = value;
    updateWindowTitle();
}

void EditorWindow::updateWindowTitle()
{
    setWindowTitle(QString("%1%2 - IDE ImaginaryC").arg(ui->editor->documentTitle()).arg(isDirty ? "*" : ""));
}

void EditorWindow::clearErrors()
{
    ui->console->clear();
    QList<QTextEdit::ExtraSelection> selections;
    ui->editor->setExtraSelections(selections);
}

void EditorWindow::openResultsWindow()
{
    if(resultsWindow->isHidden()) {
        resultsWindow->show();
    }
    else {
        resultsWindow->raise();
        resultsWindow->activateWindow();
    }
}
