#include "ConsoleWindow.h"
#include <QDateTime>

ConsoleWindow::ConsoleWindow(QWidget* parent)
    : CodeEditor(parent)
    , printer(std::make_shared<ConsolePrinter>(this))
{
    setReadOnly(true);
}

ConsoleWindow::~ConsoleWindow()
{
    printer->console = nullptr;
}

void ConsoleWindow::breakLine()
{
    moveCursor(QTextCursor::End);
    insertPlainText("\n");
    moveCursor(QTextCursor::End);
}

void ConsoleWindow::showMessage(QString prefix, QString text, ConsoleWindow::MessageType type)
{
    QColor color = Qt::black;

    switch(type) {
        case Success:
            color = Qt::darkGreen;
            break;
        case Info:
            color = Qt::darkBlue;
            break;
        case Warning:
            color = Qt::darkYellow;
            break;
        case Error:
            color = Qt::red;
            break;
        default:
            break;
    }

    QString timestamp = QDateTime::currentDateTime().toString("[HH:mm:ss.zzz] ");
    showFormattedString(timestamp, QColor(0, 0, 0), QFont::Bold);
    showFormattedString(prefix, color, QFont::Bold);
    showFormattedString(text + "\n", Qt::black, QFont::Normal);
}

void ConsoleWindow::showFormattedString(QString text, QColor color, int weight)
{
    moveCursor(QTextCursor::End);
    setTextColor(color);
    setFontWeight(weight);
    insertPlainText(text);
}

std::shared_ptr<ILogPrinter> ConsoleWindow::getPrinter()
{
    return printer;
}

ConsoleWindow::ConsolePrinter::ConsolePrinter(ConsoleWindow* console)
    : console(console)
{}

void ConsoleWindow::ConsolePrinter::logWarning(const char* message)
{
    if(console) {
        console->showMessage("Aviso: ", QString(message), MessageType::Warning);
    }
}
