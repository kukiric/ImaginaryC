#ifndef CODEEDITOR_H
#define CODEEDITOR_H

#include <QTextEdit>

class QKeyEvent;

class CodeEditor : public QTextEdit
{
    Q_OBJECT

public:
    explicit CodeEditor(QWidget* parent = 0);
    ~CodeEditor();

protected:
    // QWidget interface
    void keyPressEvent(QKeyEvent* event) override;
    void keyReleaseEvent(QKeyEvent*) override;

private:
    QChar lastChar(const QTextCursor& cur) const;

public:
    unsigned int tabSpaces;
};

#endif // CODEEDITOR_H
